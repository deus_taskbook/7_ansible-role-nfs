# 1. Создать хранилище NSF на сервере, указав экспортируемые директории
Done
# 2. Поднять на другом сервере клиентскую часть и подключить удаленные ресурсы
Done
# 3. Разместить файлы на сервере для проверки
Done
# 4. Написать ansible роль для NFS с разделением клиент\сервер
Done
Клиенты\сервера разделяются в hosts.ini

        [nfsserver]
        51.250.81.222    ansible_user=ubuntu
        [nfsclient]
        51.250.85.123    ansible_user=ubuntu
        51.250.83.65   ansible_user=ubuntu

Экспортируемые\импортируемые папки задаются через variables
        ---
        nfs_shared_dir: /exports
        nfs_mounted_dir: /mnt/nfs
# 5. Подключить NFS в качестве StorageClass для k8s
# 6. Создать PersistentVolume для сервиса в k8s, использующий п.5